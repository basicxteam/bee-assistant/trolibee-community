# TroliBee community

Repository này là kênh chia sẻ với cộng đồng về những gì chúng tôi xây dựng ở TroliBee. Tại dây bạn sẽ có thể:

* Tìm được các hướng dẫn để sử dụng TroliBee một cách hiệu quả
* Phản hồi về những tính năng chúng tôi đã release / đóng góp ý kiến và post bug để chúng tôi cải thiện thêm sản phẩm và phục vụ bạn tốt hơn.
* Được chia sẻ về định hướng phát triển sản phẩm và roadmap của chúng tôi

